package org.spiralarms.proglab8;

import java.util.Comparator;

public class TourComparator implements Comparator<Tour> {
    @Override
    public int compare(Tour t1, Tour t2) {
        return t1.getTourName().compareTo(t2.getTourName());
    }
}
