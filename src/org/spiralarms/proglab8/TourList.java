package org.spiralarms.proglab8;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class TourList {
    private ArrayList<Tour> list = new ArrayList<>();
    private TourComparator cmp = new TourComparator();
    private String fileName;
    private int lowerBound;
    private int upperBound;
    private boolean isDescending = false;


    public TourList(String fileName, int lowerBound, int upperBound, boolean isDescending) throws ClassNotFoundException, IOException {
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.fileName = fileName;
        this.isDescending = isDescending;
        try {
            loadFromFile();
        } catch (FileNotFoundException ignored) {

        }
    }

    public void loadFromFile() throws IOException, ClassNotFoundException {
        try (RandomAccessFile file = new RandomAccessFile(fileName, "r")) {
            long pos;
            while ((pos = file.getFilePointer()) < file.length()) {
                file.seek(pos);
                int length = file.readInt();
                byte[] what = new byte[length];
                file.read(what);
                ByteArrayInputStream bufIn = new ByteArrayInputStream(what);
                Object obj;
                try (ZipInputStream zis = new ZipInputStream(bufIn)) {
                    zis.getNextEntry();
                    try (ObjectInputStream ois = new ObjectInputStream(zis)) {
                        obj = ois.readObject();
                    }
                }
                Tour tour = (Tour) obj;
                list.add(tour);
            }
        }
    }

    public void add(Tour t) {
        list.add(t);
        sort();
    }

    private void sort() {
        list.sort(cmp);
    }

    public Tour get(int index) {
        return list.get(index);
    }

    public void delete(int index) {
        list.remove(index);
    }

    public int size() {
        return list.size();
    }

    @Override
    public String toString() {
        sort();
        StringBuilder sb = new StringBuilder("Tour List:\n");
        if (isDescending) {
            for (int i = min(list.size() - 1, upperBound); i >= max(0, lowerBound); --i) {
                sb.append(i).append(" ").append(list.get(i)).append("\n");
            }
        }
        else {
            for (int i = max(0, lowerBound); i <= min(list.size() - 1, upperBound); ++i) {
                sb.append(i).append(" ").append(list.get(i)).append("\n");
            }
        }
        return sb.toString();
    }

    public void saveToFile() throws IOException {
        File f = new File(fileName);
        f.delete();
        try (RandomAccessFile raf = new RandomAccessFile(fileName, "rw")) {
            for (Object tour : list) {
                long result = raf.length();
                raf.seek(result);
                ByteArrayOutputStream bufOut = new ByteArrayOutputStream();
                byte objBytes[] = objectToByteArray(tour, bufOut);
                raf.writeInt(objBytes.length);
                raf.write(objBytes);
                raf.setLength(raf.getFilePointer());
            }
        }
    }

    private static byte[] objectToByteArray(Object obj, ByteArrayOutputStream bufOut) throws IOException {
        try (ZipOutputStream zos = new ZipOutputStream(bufOut)) {
            zos.putNextEntry(new ZipEntry("r"));
            try (ObjectOutputStream oos = new ObjectOutputStream(zos)) {
                oos.writeObject(obj);
                oos.flush();
                zos.closeEntry();
                zos.flush();
                return bufOut.toByteArray();
            }
        }
    }
}
