package org.spiralarms.proglab8;

import java.io.Serializable;
import java.util.Scanner;

public class Tour implements Serializable {
    String tourName;
    String clientName;
    int nDays;
    int price;
    int commutePrice;
    int tourPrice;

    public Tour(String tourName, String clientName, int nDays, int price, int commutePrice, int tourPrice) {
        this.tourName = tourName;
        this.clientName = clientName;
        this.nDays = nDays;
        this.price = price;
        this.commutePrice = commutePrice;
        this.tourPrice = tourPrice;
    }

    public static Tour readTour(Scanner in) {
        System.out.println("Enter tour name: ");
        if (!in.hasNext()) return null;
        String tourName = in.nextLine();
        System.out.println("Enter client name: ");
        if (!in.hasNext()) return null;
        String clientName = in.nextLine();
        System.out.println("Enter the number of days: ");
        if (!in.hasNext()) return null;
        int nDays = Integer.parseInt(in.nextLine());
        System.out.println("Enter price per day: ");
        if (!in.hasNext()) return null;
        int price = Integer.parseInt(in.nextLine());
        System.out.println("Enter commute price: ");
        if (!in.hasNext()) return null;
        int commutePrice = Integer.parseInt(in.nextLine());
        System.out.println("Enter tour price: ");
        if (!in.hasNext()) return null;
        int tourPrice = Integer.parseInt(in.nextLine());

        return new Tour(tourName, clientName, nDays, price, commutePrice, tourPrice);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "tourName='" + tourName + '\'' +
                ", clientName='" + clientName + '\'' +
                ", nDays=" + nDays +
                ", price=" + price +
                ", commutePrice=" + commutePrice +
                ", tourPrice=" + tourPrice +
                '}';
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public int getnDays() {
        return nDays;
    }

    public void setnDays(int nDays) {
        this.nDays = nDays;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCommutePrice() {
        return commutePrice;
    }

    public void setCommutePrice(int commutePrice) {
        this.commutePrice = commutePrice;
    }

    public int getTourPrice() {
        return tourPrice;
    }

    public void setTourPrice(int tourPrice) {
        this.tourPrice = tourPrice;
    }
}
