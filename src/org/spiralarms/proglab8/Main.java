package org.spiralarms.proglab8;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    private static int after = Integer.MIN_VALUE;
    private static int before = Integer.MAX_VALUE;
    private static boolean descending = false;
    public static void main(String[] args) {
        for (int argc = 0; argc < args.length; ++argc) {
            if (args[argc].compareTo("-a") == 0 || args[argc].compareTo("--add") == 0) {
                processAddTour(args[argc + 1]);
                argc += 1;
            } else if (args[argc].compareTo("-l") == 0 || args[argc].compareTo("--list") == 0) {
                processListFileContents(args[argc + 1]);
                argc += 1;
            } else if (args[argc].compareTo("-d") == 0 || args[argc].compareTo("--delete") == 0) {
                processDeleteTour(args[argc + 1], args[argc + 2]);
                argc += 2;
            }
            else if (args[argc].compareTo("-h") == 0 || args[argc].compareTo("--help") == 0) {
                processPrintHelp(0);
            }
            else if (args[argc].compareTo("+i") == 0 || args[argc].compareTo("--after") == 0) {
                after = Integer.parseInt(args[argc + 1]);
                argc += 1;
            }
            else if (args[argc].compareTo("-i") == 0 || args[argc].compareTo("--before") == 0) {
                before = Integer.parseInt(args[argc + 1]);
                argc += 1;
            }
            else if (args[argc].compareTo("-r") == 0 || args[argc].compareTo("--reverse") == 0) {
                descending = true;
            }
            else {
                processPrintHelp(1);
            }
        }
    }

    private static TourList loadTourList(String fileName) {
        try {
            return new TourList(fileName, after, before, descending);
        } catch (ClassNotFoundException e) {
            System.err.println("Error: class not found");
        } catch (IOException e) {
            System.err.println("Error: io error");
        }
        System.exit(0);
        return null;
    }

    private static void saveTourList(TourList tl) {
        try {
            tl.saveToFile();
        } catch (IOException e) {
            System.err.println("Error: io error");
        }
    }

    private static void processDeleteTour(String strIndex, String fileName) {
        int index = Integer.parseInt(strIndex);
        TourList tl = loadTourList(fileName);
        try {
            tl.delete(index);
        } catch (IndexOutOfBoundsException e) {
            if (tl.size() == 0)
                System.out.println("Tour list is empty");
            else
                System.out.println("Specify index in range 0-" + (tl.size() - 1));
        }
        saveTourList(tl);
    }

    private static void processListFileContents(String fileName) {
        TourList tl = loadTourList(fileName);
        System.out.println(tl);
    }

    private static void processAddTour(String fileName) {
        TourList tl = loadTourList(fileName);
        Scanner in = new Scanner(System.in);
        Tour t = Tour.readTour(in);
        tl.add(t);
        saveTourList(tl);
    }

    private static void processPrintHelp(int exitCode) {
        System.out.printf("View/modify binary files containing tour information.\n\nUsage:\n  java %s\n\nOptions:\n  -h, --help\t\t\t\tdisplay help message\n  -a, --add [file]\t\t\tadd new tour information to binary file\n  -d, --delete [index] [file]\t\tdelete tour by index\n  -l, --list [file]\t\t\tlist contents of the binary file\n    -i, --before [index]\t\t\twork with indices <= [index]\n    +i, --after [index]\t\t\t\twork with indices >= [index]\n    -r, --reverse\t\t\t\treverse sorting order\n", Main.class.getCanonicalName());
        System.exit(exitCode);
    }
}
